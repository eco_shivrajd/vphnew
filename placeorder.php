<?php  ini_set('display_errors', 1); 
include 'db.class.php';
$json_string =$_POST['orderarray'];
$array = json_decode($json_string);
$db = new DB();
/*
if($array->dist_name!="")
	$dist_name = $array->dist_name;
else
	$dist_name = "No Distributor";
*/
$sales_person_name = $array->sales_person_name;
$fullnm=$array->sales_person_full_name;
$sales_person_id = $array->sales_person_id;
//$dist_id = $array->dist_id;
$super_stock_id = $array->super_stockist_id;
if($array->super_stockist_name!="")
	$super_stockist_name=$array->super_stockist_name;
else
	$super_stockist_name="No superstockist";

$total_products = 0;
$order_date = $array->order_date;


$oplacelat=$array->usr_lng;
$oplacelon=$array->usr_lat;   
$total_order_cost = 0;
$sale_count = 0;
$free_count = 0;
foreach($array->order_list as $key => $order)
{
   $order_id = $order->order_id;
   $shop_name = $order->shop_name;
   $total_items=$order->total_items;
   $total_costs=$order->total_cost;
   $shop_id = $order->shop_id;
   $dist_id = $order->dist_id;
    
   if($order->dist_name!="")
	$dist_name = $order->dist_name;
	else
	$dist_name = "No Distributor";
	if(isset($order->product_details))
	{
	   foreach($order->product_details as $pkey => $product)
	   {
		$product->quantity = 1;
		$productid = $product->product_id;
		$productnm = "test";
		$catnm=$product->category_name;
		$catid=$product->category_id;
		$brandnm=$product->brand_name;
		$brandid=$product->brand_id;
		$orderappid = $db->add_order($productnm, $dist_name, $sales_person_name, $product->quantity,$shop_name,$order_date,$sales_person_id,$dist_id,$super_stock_id,$super_stockist_name,$catnm,$catid,$brandid,$brandnm,$fullnm,$oplacelat,$oplacelon,$shop_id);

		$total_products++;
		$product_qty = 0;
		$offer_provided = 1;
	   
	   foreach($product->varient_details as $vkey => $varient) {
			$campaign_type = "";
			$campaign_sale_type = "";
			if($varient->campaign_applied == 'yes')
			{
				$campaign_type = $varient->campaign_type;
				if($varient->campaign_type == 'free_product')
				{
					$offer_provided = 0;
					$campaign_sale_type = 'sale';
					
					if(isset($varient->campaign_free_product_details))
					{
						 $c_p_order_variant_id = $db->add_varient($orderappid, $varient->weight, $varient->weightquantity, $varient->unit, $varient->size, $varient->quantity, $shop_id, $shop_name, $order_id, $varient->price, $varient->product_variant_rowcnt,$productid, $productnm, $varient->campaign_applied, $campaign_type, $campaign_sale_type);
							$total_order_cost+=($varient->price * $varient->quantity);		
							$sale_count+= 1;
						 if(count($varient->campaign_free_product_details) > 0)
						 {
							 $campaign_sale_type = 'free';
							
							 foreach($varient->campaign_free_product_details as $fkey => $fvarient) {//echo "in";
								if($fvarient->super_stockist_name!="")
								   $super_stockist_name_free_pro=$fvarient->super_stockist_name;
								else
									$super_stockist_name_free_pro="No superstockist";
								
								if($fvarient->productid != $productid)
								{	
									$orderappid_free_pro = $db->add_order($productnm, $dist_name, $fvarient->sales_person_name, $fvarient->quantity,$fvarient->shop_name,$order_date,$fvarient->sales_person_id,$fvarient->dist_id,$fvarient->super_stockist_id,$super_stockist_name_free_pro,$fvarient->category_name,$fvarient->category_id,$fvarient->brand_id,$fvarient->brand_name,$fvarient->sales_person_full_name,$fvarient->usr_lng,$fvarient->usr_lat,$fvarient->shop_id);
								}
								else
									$product_qty += $fvarient->quantity;
								
								$free_count+= 1;
								$f_p_order_variant_id = $db->add_varient($orderappid_free_pro, $fvarient->weight, $fvarient->weightquantity, $fvarient->unit, $fvarient->size, $fvarient->quantity, $fvarient->shop_id, $fvarient->shop_name, $order_id, $fvarient->price, $fvarient->product_variant_rowcnt,$fvarient->product_id, $productnm, $varient->campaign_applied, $campaign_type, $campaign_sale_type);
								
								$db->add_order_c_n_f_product($varient->campaign_id,$c_p_order_variant_id,$f_p_order_variant_id);
							 } //exit;	
						 }
					}		
							
				} /*else if($varient->campaign_type == 'by_weight')
				{
					$campaign_sale_type = 'free';
					$db->add_varient($orderappid, $varient->weight, $varient->weightquantity, $varient->unit, $varient->size, $varient->quantity, $shop_id, $shop_name, $order_id, $varient->price, $varient->product_variant_rowcnt,$productid, $productnm, $varient->campaign_applied, $campaign_type, $campaign_sale_type);
				}*/
				else{
					$offer_provided = 0;
					$discounted_amount = $varient->price - $varient->campaign_discount;
					$total_order_cost+=($discounted_amount * $varient->quantity);
					$sale_count+= 1;
					$order_variant_id = $db->add_varient($orderappid, $varient->weight, $varient->weightquantity, $varient->unit, $varient->size, $varient->quantity, $shop_id, $shop_name, $order_id, $discounted_amount,$varient->product_variant_rowcnt, $productid, $productnm, 'yes', $campaign_type, $campaign_sale_type);
					
					$db->add_order_c_p_discount($varient->campaign_id,$order_variant_id,$varient->campaign_discount,$varient->campaign_percent,$varient->price);
				}
			} else {
				$total_order_cost+=($varient->price * $varient->quantity);
				$sale_count+= 1;
				$db->add_varient($orderappid, $varient->weight, $varient->weightquantity, $varient->unit, $varient->size, $varient->quantity, $shop_id, $shop_name, $order_id, $varient->price,$varient->product_variant_rowcnt, $productid, $productnm);
			}
			$product_qty += $varient->quantity;
	   }
	   $db->update_order($orderappid, $product_qty, $offer_provided);
	   }
	}
	if(isset($order->product_details_weight) && count($order->product_details_weight) > 0)
	{
	   foreach($order->product_details_weight as $pkey => $product)
	   {			
			$productid = $product->product_id;
			$productnm = "test";
			$catnm=$product->category_name;
			$catid=$product->category_id;
			$brandnm=$product->brand_name;
			$brandid=$product->brand_id;
			$orderappid = $db->add_order($productnm, $dist_name, $sales_person_name, $product->quantity,$shop_name,$order_date,$sales_person_id,$dist_id,$super_stock_id,$super_stockist_name,$catnm,$catid,$brandid,$brandnm,$fullnm,$oplacelat,$oplacelon,$shop_id);

			$total_products++;
			$product_qty = 0;
			$offer_provided = 1;
			//foreach($product->varient_details as $vkey => $varient) {
				$campaign_type = "by_weight";
				$campaign_sale_type = "";
				if($product->campaign_applied == 'yes')
				{
					if($product->campaign_type == 'by_weight')
					{
						$offer_provided = 0;
						$campaign_sale_type = 'free';
						$order_variant_id = $db->add_varient($orderappid, $product->weight, $product->weightquantity, $product->unit, $product->size, $product->quantity, $shop_id, $shop_name, $order_id, $product->price, $product->product_variant_rowcnt,$productid, $productnm, $product->campaign_applied, $campaign_type, $campaign_sale_type);
						$product_qty += $product->quantity;
					}
				}
			//}
			$free_count+= 1;
			$db->add_order_purchase_p_weight($product->campaign_id,$order_variant_id,$product->total_wt_purchase,$product->total_wt_purchase_unit);
			$db->update_order($orderappid, $product_qty, $offer_provided);
	   }
	}
}
include 'wsJSON.php';
$JSONVar = new wsJSON($con);
$total_count = $sale_count + $free_count;
$jsonOutput = $JSONVar->fnplaceorder($total_order_cost,$sale_count,$free_count,$total_count);
echo $jsonOutput;
