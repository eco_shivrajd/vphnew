<?php
/***********************************************************
 * File Name	: orderManage.php
 ************************************************************/	
class orderManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function getReportTitle($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = '';
		switch($report_type){
			case "superstockist":
					$report_title = 'Super Stockist';
				break;
			case "stockist":
					$report_title = 'Stockist';
				break;
			case "salesperson":	
					$report_title = 'Sales Person';
				break;
			case "shop":	
					$report_title = 'Shops';
				break;
			case "product":	
					$report_title = 'Products';
				break;
		}
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	public function getAllOrders() {
		extract($_POST);	
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($report_type){
			case "superstockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' $search_name
							GROUP BY u.firstname  ".$order_by.$limit;
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name
							GROUP BY u.firstname ".$order_by.$limit;
						break;
						 
						break;
					}
				break;
			case "stockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Superstockist":						
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' AND u.external_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						 
						break;
					}
				break;
			case "salesperson":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id, u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE u.user_type='SalesPerson' $search_name GROUP BY u.firstname ".$order_by.$limit;	
							
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id =  '".$_SESSION[SESSION_PREFIX.'user_id']."' OR u.external_id IN (SELECT id FROM tbl_user where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."') ) 
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id =  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name   AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
					}
				break;
			case "shop":	
					if($search != '')
						$search_name = " AND s.name LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":						
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) ".$condnsearch." 
							WHERE s.id!=0 AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE s.id!=0 AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !=''  $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id  AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE s.id!=0  AND  s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
							
						break;
					}	
				break;
			case "product":	
					if($search != '')
						$search_name = " AND p.productname LIKE '%".$search."%'";
				switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":		
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))  AND vo.productid = p.id 
							WHERE p.id!=0 AND p.productname !=''  $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Superstockist":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Distributor":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name  
							GROUP BY p.productname ".$order_by.$limit;							
						break;
					}	
				break;
			default:
				break;
		}
		
		
		
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			if($sql2 != ''){
				$result2 = mysqli_query($this->local_connection,$sql2);
				$j = 1;
				$row_count2 = mysqli_num_rows($result2);
				if($row_count2 > 0){
					$name = array_column($records,'Name') ;
					
					while($row2 = mysqli_fetch_assoc($result2))
					{
						if(!in_array($row2['Name'],$name))
						{
							$records2[$j] = $row2;
							$j++;
						}
					}				
				}else{
					$records2 =0;
				}
			}
			if($records2 != 0)
			{
				$records = array_merge($records,$records2);				
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function getReportTitleForSP($selTest=null,$frmdate=null,$todate=null){	
		$report_title = '';		
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$frmdate = date('d-m-Y',$frmdate);
					$todate = date('d-m-Y',$todate);
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	function getSPShopOrdersSummary($sp_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u 				
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Superstockist":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u 
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Distributor":		
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.variantunit * vo.totalcost) as Total_Sales
				FROM tbl_user u  
				LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	function getSPShopOrders($sp_id, $s_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";					
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";					
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";					
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 				
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid)
				WHERE oa.order_by = $sp_id AND oa.shop_id = $s_id ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Superstockist":
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 		
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid )
				WHERE oa.order_by = $sp_id AND vo.status != 1 AND oa.shop_id = $s_id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Distributor":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_varient_id, vo.orderappid, vo.variantunit, vo.totalcost, vo.id as variant_oder_id, oa.order_date, vo.orderid, vo.productid, vo.campaign_sale_type
				FROM tbl_order_app oa 		
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid)
				WHERE oa.order_by = $sp_id AND oa.shop_id = $s_id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";										
				$product_result = mysqli_query($this->local_connection,$sql_product);
				$obj_product = mysqli_fetch_object($product_result);
				$product_varient_id = $row['product_varient_id'];
				$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
				$resultprd = mysqli_query($this->local_connection,$sqlprd);
				$rowprd = mysqli_fetch_array($resultprd);
				$exp_variant1 = $rowprd['variant_1'];
				$imp_variant1= split(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2']; $imp_variant2= split(',',$exp_variant2);
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit1 = $rowunit['unitname'];
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit2 = $rowunit['unitname'];
				$dimentionDetails = "";
				if($imp_variant1[0]!="") {
					$dimentionDetails = " - Pcs: " .  $imp_variant1[0] . " " . $variant_unit1;
				}
				$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
				$row['prodname_variant'] = $obj_product->productname . $dimentionDetails;
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function no_order_history_details($sp_id=null,$filter_date=null,$date1=null,$date2=null) {
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');					
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;				
				default:
					$condnsearch_sp="";
				break;

			}
		}
		if($sp_id!=''){
			$where = " AND salesperson_id = $sp_id";
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
					$sql1="SELECT `id`, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit 
					WHERE shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Superstockist":
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Distributor":	
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
		}
		//echo $sql1; exit;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
}
?>