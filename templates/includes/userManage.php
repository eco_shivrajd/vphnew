<?php
/***********************************************************
 * File Name	: userManage.php
 ************************************************************/	
class userManager 
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function getAllLocalUserDetails($user_type,$external_id=''){
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id IN (". $external_id.")";
		}
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, `pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, `reset_key`, `suburbid`, `is_default_user`
		FROM tbl_user where user_type = '".$user_type."' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getLocalUserDetailsByUserType($user_type,$external_id='') {
		$where_clause = '';
		if($external_id != '')
		{
			$where_clause = " AND external_id = ". $external_id;
		}
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, `pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, `reset_key`, `suburbid`, `is_default_user`
		FROM tbl_user where user_type = '".$user_type."' $where_clause order by is_default_user desc, firstname asc";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	public function getLocalUserDetails($user_id) {
		$sql1="SELECT `id`, `external_id`, `surname`, `firstname`, `username`, `pwd`, `user_type`, `address`, `city`, `state`, `mobile`, `email`, `reset_key`, `suburbid`, `is_default_user`
		FROM tbl_user where id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getLocalUserWorkingAreaDetails($user_id){
		$sql1="SELECT `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` FROM tbl_user_working_area where user_id = '".$user_id."'";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;	
	}
	public function getCommonUserDetails($user_id) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where id = '".$user_id."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}
	public function getCommonUserDetailsByUsername($username) {
		$sql1="SELECT `uid`, `emailaddress`, `passwd`, `username`, `level`, `reset_key`, `id`, `address`, `state`, `city`, `mobile`, `firstname`, `suburbid` 
		FROM tbl_users where username='".$username."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;			
	}
	public function getCommonUserCompanyDetails($user_id) {
		$sql1="SELECT `id`, `userid`, `companyid` FROM tbl_user_company where userid='".$user_id."' AND companyid='".COMPID."'";
		$result1 = mysqli_query($this->common_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;
	}
	public function addLocalUserDetails($user_type) {
		extract ($_POST);
		$external_id =	$_SESSION[SESSION_PREFIX.'user_id'];	
		
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
		}
		$surname	=	"";
		$name		=	fnEncodeString(trim($firstname));
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$email		=	fnEncodeString($email);
		$mobile		= 	fnEncodeString($mobile);		
		$address	= 	fnEncodeString($address);		
		
		$fields = '';
		$values = ''; 
	
		if($email != '')
		{
			$fields.= ",`email`";
			$values.= ",'".$email."'";
		}
		if($mobile != '')
		{
			$fields.= ",`mobile`";
			$values.= ",'".$mobile."'";
		}
		if($address != '')
		{
			$fields.= ",`address`";
			$values.= ",'".$address."'";
		}
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		
		$user_sql = "INSERT INTO tbl_user (`external_id`,`surname`,`firstname`,`username`,`pwd`,`user_type` $fields) 
		VALUES('".$external_id."','".$surname."','".$name."','".$username."','".$password."','".$user_type."' $values)";
		mysqli_query($this->local_connection,$user_sql);		
		return $userid=mysqli_insert_id($this->local_connection); 	
	}
	public function addLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state_ids`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city_ids`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_ids`";
			$suburb_ids = implode(',',$area);
			$values.= ",'".$suburb_ids."'";
		}
		if($subarea != '')
		{
			$fields.= ",`subarea_ids`";
			$subarea_ids = implode(',',$subarea);
			$values.= ",'".$subarea_ids."'";
		}
		
		$user_working_area = "INSERT INTO tbl_user_working_area (`user_id` $fields) 
		VALUES('".$user_id."' $values)";
		mysqli_query($this->local_connection,$user_working_area);
	}
	public function addCommonUserDetails($user_type,$user_id) {
		extract ($_POST);
		$email		=	fnEncodeString($email);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$fields = '';
		$values = ''; 
		if($state != '')
		{
			$fields.= ",`state`";
			$values.= ",'".$state."'";
		}
		if($city != '')
		{
			$fields.= ",`city`";
			$values.= ",'".$city."'";
		}
		$user_sql = "INSERT INTO tbl_users (`id`, `emailaddress`, `passwd`, `username`, `level` $fields) 
		VALUES('".$user_id."','".$email."','".$password."','".$username."','".$user_type."' $values)";
		mysqli_query($this->common_connection,$user_sql);	
		return $userid=mysqli_insert_id($this->common_connection); 		
	}
	public function addCommonUserCompanyDetails($user_id) {		
		$company_sql = "INSERT INTO tbl_user_company(userid,companyid)  VALUES('".$user_id."','".COMPID."')";
		mysqli_query($this->common_connection,$company_sql);
	}
	public function sendUserCreationEmail() {
		extract ($_POST);
		$username	=	fnEncodeString($username);
		$password	=	fnEncodeString($password);
		$password	=	md5($password);
		$subject   = "Account Created";
		$fromMail  = FROMMAILID;		
		$headers = "";
		$headers .= "From: ".$fromMail."\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-Type: text/html;" . "\r\n";
		$message = "";
		$message .= "Hi, <br/>";
		$message .= "Your account created successfully<br/>";
		$message .= "Please find the login details below<br/>";
		$message .= "Username: $username<br/>";
		$message .= "Password: $rand<br/>";
		$message .= "<a href='".SITEURL."templates/login.php'>Click Here</a> to Login<br/>";
		$message .= "<br/><br/>";
		$message .= "Thanks,<br/>";
		$message .= "Admin.";
		if($username != ""){
			$sent = @mail($username,$subject,$message,$headers);
		}
	}
	public function updateLocalUserDetails($user_type, $user_id) {	
		extract ($_POST);	
		$address= fnEncodeString($address);	
		$email	= fnEncodeString($email);
		$mobile	= fnEncodeString($mobile);			
		$values = '';
		
		$values.= " `id`= '".$user_id."'";
			
		if($address != '')
		{
			$values.= ", `address`= '".$address."'";				
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		if($email != '')
		{
			$values.= ", `email`= '".$email."'";				
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if(($user_type == 'Distributor' OR $user_type == 'SalesPerson') && $assign != "")
		{
			$external_id = $assign;
			$values.= ", `external_id`= '".$external_id."'";
		}
		//if($_SESSION[SESSION_PREFIX.'user_type'] == 'Superstockist' OR $_SESSION[SESSION_PREFIX.'user_type'] == 'Distributor')
		//{
			if($firstname!="") {
				$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";	
			}
			if($username!="") {
				$values.= ", `username`= '".fnEncodeString($username)."'";					
			}
			
		//}
		
		$update_user_sql = "UPDATE tbl_user SET $values WHERE id='$user_id'";
		mysqli_query($this->local_connection,$update_user_sql);
	}
	
	public function updateCommonUserDetails($user_id) {		
		extract ($_POST);	
		if($firstname != '')
		{
			$values.= ", `firstname`= '".fnEncodeString(trim($firstname))."'";
		}
		if($username != '')
		{
			$values.= ", `username`= '".fnEncodeString($username)."'";
		}
		if($email != '')
		{
			$values.= ", `emailaddress`= '".fnEncodeString($email)."'";
		}
		if($mobile != '')
		{
			$values.= ", `mobile`= '".$mobile."'";				
		}
		if($address != '')
		{
			$values.= ", `address`= '".fnEncodeString($address)."'";
		}
		if($state != '')
		{
			$values.= ", `state`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city`= '".$city."'";
		}
		$update_user_sql = "UPDATE tbl_users SET id='$user_id' $values WHERE id='$user_id'";
		mysqli_query($this->common_connection,$update_user_sql);		
	}
	
	public function updateLocalUserWorkingAreaDetails($user_id) {
		extract ($_POST);		
		$values = ''; 
		if($state != '')
		{
			$values.= ", `state_ids`= '".$state."'";
		}
		if($city != '')
		{	
			$values.= ",`city_ids`= '".$city."'";
		}
		if($area != '')
		{
			$suburb_ids = implode(',',$area);
			$values.= ", `suburb_ids`= '".$suburb_ids."'";
		}else{
			$values.= ", `suburb_ids`= ''";
		}
		if($subarea != '')
		{			
			$subarea_ids = implode(',',$subarea);
			$values.= ",`subarea_ids`= '".$subarea_ids."'";
		}else{
			$values.= ",`subarea_ids`= ''";
		}
			
		$user_working_area = "UPDATE tbl_user_working_area SET user_id='$user_id' $values WHERE user_id='$user_id'";				
			
		mysqli_query($this->local_connection,$user_working_area);
	}
	function checkUserBelongTo($user_id,$external_id){
		if($external_id == $_SESSION[SESSION_PREFIX.'user_id'])
			return 0;
		else{
			$user_data = $this->getLocalUserDetails($external_id);
			if($user_data['external_id'] == $_SESSION[SESSION_PREFIX.'user_id'])
				return 0;
			else
				return 1;
		}
	}
}
?>