<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Sales Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Sales Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Sales Persons Listing
							</div>
                            <a href="sales-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Sales Person
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									 Name
								</th>								
                                <th>
									 Email
								</th>
                                <th>
									 Mobile Number
								</th>
                                <th>
                                	City
                                </th>
                                <th>
                                	State
                                </th>
							</tr>
							</thead>
							<tbody>
							<?php
							
							switch($_SESSION[SESSION_PREFIX.'user_type']){
								case "Admin":
									
									$user_type="SalesPerson";
									//$sql="SELECT * FROM `tbl_user` where user_type ='$user_type'";
									$external_id = "";
									break;
								case "Superstockist":
									
									$user_typedis="Distributor";
									$getdistrib="SELECT GROUP_CONCAT(id) as ids FROM `tbl_user` where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."' AND user_type ='$user_typedis' group by external_id";

									$resultdistrib = mysqli_query($con,$getdistrib);
									$rowdistrib = mysqli_fetch_array($resultdistrib);
									$variable=explode(",", $rowdistrib['ids']);
									$variable1 = "'".implode("','", $variable)."'";
									$tuser=$variable1.",'".$_SESSION[SESSION_PREFIX.'user_id']."'";
									$user_type="SalesPerson";
									//$sql="SELECT * FROM `tbl_user` where external_id IN(".$tuser.") AND user_type='".$user_type."'";
									$external_id = $tuser;
									
								break;
								case "Distributor":
								
									$user_type="SalesPerson";
									//$sql="SELECT * FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION[SESSION_PREFIX.'user_id']."'";
									$external_id = $_SESSION[SESSION_PREFIX.'user_id'];
								break;
							}
							
							$result1 = $userObj->getAllLocalUserDetails($user_type,$external_id);
							//$result1 = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result1))
							{
								$working_area_details = $userObj->getLocalUserWorkingAreaDetails($row['id']);
								if($working_area_details == 0){								
									$row['state_ids'] = $row['state'];									
									$row['city_ids'] = $row['city'];
								}elseif(count($working_area_details) > 0){	
									$row = array_merge($row,$working_area_details);
								}
						
								echo '<tr class="odd gradeX">
								<td>
									 <a href="sales1.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a>
								</td>'; 
                              
								echo '<td>'.$row['email'].'</td>
                                <td>'.$row['mobile'].'</td>
                                <td>';
								
								$city_id=$row['city_ids'];
								if(!empty($city_id)){
									$sql="SELECT * FROM tbl_city where id = $city_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									} 
								}else{
									echo '-';
								}
								echo '</td><td>';
								$state_id=$row['state_ids'];
								if(!empty($state_id)){
									$sql="SELECT * FROM tbl_state where id = $state_id";
									$result = mysqli_query($con,$sql);
									while($num = mysqli_fetch_array($result))
									{ 
										echo  $num['name'];
									}
								}else{
									echo '-';
								}
								echo '</td> </tr>';
							} ?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>