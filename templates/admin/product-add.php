<?php
 include "../includes/header.php";
$sqlvariant1="SELECT name FROM tbl_variant WHERE id='40'";
$resultprd1 = mysqli_query($con,$sqlvariant1);
$rowvariant1 = mysqli_fetch_array($resultprd1);
//////////////////////////////////////////////////////////////////////
$sqlvariant2="SELECT name FROM tbl_variant WHERE id='41'";
$resultprd2 = mysqli_query($con,$sqlvariant2);
$rowvariant2 = mysqli_fetch_array($resultprd2);
/////////////////////////////////////////////////////////////////////
if(isset($_POST['submit']))
{
	//print_r($_POST);
	$price=$_POST['price'];
	$variant1=$_POST['variant1'];
	$variant2=$_POST['variant2'];
	$countobj=count($price);
	
	$catid=$_POST['catid'];
	$productname=fnEncodeString($_POST['productname']);
	
	$sql_product_check=mysqli_query($con,"select id from `tbl_product` where productname='$productname'");
	
	if($rowcount = mysqli_num_rows($sql_product_check)>0){	
		echo '<script>alert("Duplicate Product not added.");location.href="product-add.php";</script>';
	}else{
		$sql_product_add=mysqli_query($con,"INSERT INTO `tbl_product` (catid,productname) VALUES('$catid','$productname')");	
		$sql_id= mysqli_insert_id($con);
		for($i=0;$i<$countobj;$i++){				
				$var1= $variant1[$i*2].','.$variant1[($i*2)+1];
				$var2= $variant2[$i*2].','.$variant2[($i*2)+1];
				$pri1=$price[$i];
				"INSERT INTO `tbl_product_variant`(productid,variant_1,variant_2,price,variant_cnt) VALUES ('$sql_id','$var1','$var2','$pric',1)";
				$sql3=mysqli_query($con,"INSERT INTO `tbl_product_variant`(productid,variant_1,variant_2,price,variant_cnt) VALUES ('$sql_id','$var1','$var2','$price','$i')");
		}
		echo '<script>alert("Product added successfully.");location.href="product.php";</script>';
	}
	
	
	exit;
	
} ?>
<!-- BEGIN HEADER -->
 
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Product";
	include "../includes/sidebar.php";
	?>
	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Product
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="product.php">Product</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Product</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Product
							</div>
							
						</div>
						<div class="portlet-body">
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="product-add.php">
          
            
            <div class="form-group">
              <label class="col-md-3">Category:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <select name="catid"
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select category"
				class="form-control">
                <option  selected disabled>-Select-</option>
				<?php
				$sql="SELECT c.id as id,b.name, c.categorynm FROM `tbl_category` c,tbl_brand b WHERE b.id=c.brandid";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
				$id=$row['id'];
				echo "<option value='$id'>".fnStringToHTML($row['name'])."-" . fnStringToHTML($row['categorynm']) . "</option>";
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Product Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" name="productname"
                placeholder="Enter Product Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter product name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				class="form-control">
              </div>
            </div><!-- /.form-group -->
		<div id="prodvar">
            <div class="form-group">
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant1['name']);?>:</label>
		          <div class="col-md-4 nopadl">		  

              <div class="col-sm-4">
                <input type="text" name="variant1[]" id="numunits1" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9-!@#$%^&*+_=><,./:' ]*$" class="form-control">
              </div>
              
              <div class="col-md-4">
                <select name="variant1[]" id="units_variant_id1"  class="form-control">
				<?php 
				$sql_id="SELECT  * FROM `tbl_units_variant` WHERE variantid =40";
				$result_id = mysqli_query($con,$sql_id);
				while($row_id = mysqli_fetch_array($result_id))
				{
					$unitname=$row_id['unitname'];
					$sql="SELECT  unitname,id FROM `tbl_units` WHERE id='$unitname'";
					$result = mysqli_query($con,$sql);
					while($row = mysqli_fetch_array($result))
					{
						$unit=$row['id'];
						echo "<option value='$unit'>".fnStringToHTML($row['unitname'])."</option>";
					}
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->
		</div>

            <div class="form-group">
              <label class="col-md-3"><?php echo fnStringToHTML($rowvariant2['name']);?>:</label>
				<div class="col-md-4 nopadl">		
              <div class="col-md-4">
                <input type="text" name="variant2[]" id="numunits2" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[a-zA-Z0-9-!@#$%^&*+_=><,./:' ]*$" class="form-control">
              </div>
              <div class="col-md-4">
                <select name="variant2[]" id="units_variant_id2" class="form-control">
				<?
				$sql="SELECT  unitname,id FROM `tbl_units`";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					echo "<option value='".$row['id']."'>".fnStringToHTML($row['unitname'])."</option>";
				}
				?>
                </select>
              </div>
            </div><!-- /.form-group -->			          
          </div> 
			<div class="form-group">
              <label class="col-md-3">Price:</label>
              <div class="col-md-4">
                <input type="text" name="price[]" placeholder="Price" class="form-control"  
				data-parsley-trigger="change"
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9-!@#$%^&*+_=><,./:' ]*$">
              </div>
            </div><!-- /.form-group -->	
			
		</div>
			<div id="demo"></div>   
           <div class="clearfix"></div> 
		   
            
             
                  
            <hr/>      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
				<a type="button" class="btn btn-primary" id="btn1">Add More Variant</a>
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="product.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->



<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>