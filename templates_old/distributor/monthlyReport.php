<?php
//ini_set("display_errors", "1"); error_reporting(E_ALL);

include ("../connection/connection.php");

extract($_POST);
$maxDays=date('t');
$StartDate = "01-" . $drpMonthlyOption;
$MonthName = date('F', strtotime($StartDate . ' +0 day'));
$endDate = $maxDays . "-" . $drpMonthlyOption;

function getProductData($con, $counter , $dateToShow , $arrProductInfo,$arrPOST) 
{	
	extract($arrPOST);
	$returnString = "";
	$AllCat_totalquantity = 0;
	$AllCat_totalcost = 0;
	
	for($i=0;$i<$counter;$i++)
	{
		$sql = "SELECT 
			VO.unit,
			sum(VO.weightquantity) as totalquantity,
			sum(VO.totalcost*VO.variantunit) as totalcost
		FROM 
			tbl_order_app OA LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
			LEFT JOIN tbl_shops shops ON shops.id= VO.shopid	
		WHERE 		
			date_format(OA.order_date, '%d-%m-%Y') = '".$dateToShow."' ";
		
		$condition = " AND OA.catid = " . $arrProductInfo[$i]['catid'];		
		if($dropdownSalesPerson  !="")
		{
			$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
		}
		if($dropdownSalesPerson  =="")
		{
		if($dropdownStockist  !="")
		{
			$condition .= " AND OA.distributorid = " . $dropdownStockist;
		}
		}
		
		if($dropdownStockist  ==""){
		if($cmbSuperStockist !="")
		{
			$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
		}
		}
		if($dropdownshops !="")
		{
			$condition .= " AND VO.shopid = " . $dropdownshops;
		}
		

		if($dropdownbrands  !="")
		{
			$condition .= " AND OA.brandid = " . $dropdownbrands;
		}  
		
		if($dropdownProducts  !="")
		{
			$condition .= " AND VO.productid = " . $dropdownProducts;
		}
		
		if($dropdownCity !="")
		{
			$condition .= " AND shops.city = " . $dropdownCity;
		}

		if($dropdownState !="")
		{
			$condition .= " AND shops.state = " . $dropdownState;
		}

		$sql .= $condition;
		$sql .= "  group by VO.unit ";
		$result1 = mysqli_query($con,$sql);
		
		$Cat_totalquantity = 0; $Cat_totalcost = 0;
		while($row = mysqli_fetch_array($result1))
		{
			$totalquantity = floatval($row["totalquantity"]);
			if($row["unit"]=="g") {
				$totalquantity = $totalquantity / 1000;
			}
			$Cat_totalquantity = floatval($Cat_totalquantity) + floatval($totalquantity);
			$Cat_totalcost = floatval( $Cat_totalcost) + floatval($row["totalcost"]);
			
			$AllCat_totalquantity = floatval($AllCat_totalquantity) + floatval($totalquantity);
			$AllCat_totalcost = floatval($AllCat_totalcost) + floatval($row["totalcost"]);
		}
		
		$returnString .= "<td style='text-align:right;'>". $Cat_totalquantity . "</td>";
		$arrReturn["Cat_totalquantity"][$i] = floatval($Cat_totalquantity);
		$arrReturn["Cat_totalcost"][$i] = floatval($Cat_totalcost);
	}
	$returnString .= "<th style='text-align:right;'>". floatval($AllCat_totalquantity) . "</th>"; 
	$arrReturn["returnString"] = $returnString;	
	return $arrReturn;
}
?>

<div class="portlet box blue-steel">
	<div class="portlet-title">
		<div class="caption"><i class="icon-puzzle"></i>Monthly Sales Report</div>
	</div>
	<div class="portlet-body">
		<div class="table-responsive">
			<?
			if($dropdownshops !="")
			{
				$sql=" SELECT  name from tbl_shops where id =  $dropdownshops";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$MarketName = $row["name"];
			} else {
				$MarketName = "ALL";
			}
			
			if($dropdownStockist !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownStockist."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$DistributorName = $row["firstname"];
			} else {
				$DistributorName = "ALL";
			}
			
			if($dropdownSalesPerson !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownSalesPerson."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SalesPersonName = $row["firstname"];
			} else {
				$SalesPersonName = "ALL";
			}
			
			if($dropdownSuburbs !="")
			{
				$sql="SELECT suburbnm FROM tbl_surb WHERE id=$dropdownSuburbs ";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SuburbName = $row["suburbnm"];
			} else {
				$SuburbName = "ALL";
			} 
			?>
			<table class="table table-striped table-hover table-bordered" width="100%">
				<thead>
				<tr>
					<th width="50%">Sales Person: <?=$SalesPersonName;?></th>
					<th width="50%" colspan="2">From Date: <?=$StartDate;?> To Date: <?=$endDate;?> </th>
					
				</tr>
				<tr>
					<th width="50%">Distributor Name: <?=$DistributorName;?></th>
					<th width="25%">Area: <?=$SuburbName;?></th>
					<th width="25%">Month: <?=$MonthName;?></th>
				</tr>
				</thead>
			</table>
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>SR.</th>
						<th>Date</th>
						<th>SHOP</th>
						<?php	
						
						$sql=" SELECT 
							tbl_brand.name, 
							tbl_brand.type, 
							tbl_brand.id, 
							tbl_category.categorynm,
							tbl_product.catid,
							count(tbl_category.brandid) as total_categories, 
							count(tbl_product.catid) as total_products 
						FROM 
							tbl_category 
							LEFT JOIN tbl_brand on tbl_brand.id = tbl_category.brandid 
							LEFT JOIN tbl_product on tbl_product.catid = tbl_category.id  ";
							
						$condition = " Where 1=1 ";
						if($dropdownbrands  !="")
						{
							$condition .= " AND tbl_category.brandid = " . $dropdownbrands;
						} 
						if($dropdownCategory  !="")
						{
							$condition .= " AND tbl_product.catid = " . $dropdownCategory;
						}
						if($dropdownProducts  !="")
						{
							$condition .= " AND tbl_product.productid = " . $dropdownProducts;
						}
						
						$sql .= $condition;
						
						$sql .= " GROUP By categorynm having total_products > 0  ORDER BY  tbl_brand.type,  tbl_brand.name,  tbl_category.categorynm";
						
						$result1 = mysqli_query($con,$sql);
						$type = ""; 
						$i=0;
						$counter = 0;
						while($row = mysqli_fetch_array($result1))
						{
							if($type != $row["type"] && $i!=0) {
							?><th style="text-align:center;" colspan="<?=$i;?>"><?=$type;?></th><?	
							$i=0;
							}
							$type = $row["type"];
							$arrProductInfo[$counter]['brand'] = $row["name"];
							$arrProductInfo[$counter]['category'] = $row["categorynm"];
							$arrProductInfo[$counter]['catid'] = $row["catid"];
							$i++;
							$counter++;
						}
						?><th colspan="<?=$i;?>" style="text-align:center;"><?=$type;?></th>
						<th>&nbsp;</th>
					</tr>
					
					<tr>
						<th>No.</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?=$arrProductInfo[$i]['brand'];?></th><? } ?>
						<th>&nbsp;</th>
					</tr>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?=$arrProductInfo[$i]['category'];?></th><? } ?>
						<th>Total</th>
					</tr>

					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
						for($i=0;$i<$counter;$i++)
						{ ?><th style="text-align:center;">IN KG</th><? } ?>
						<th>IN KG</th>
					</tr>				
					
				</thead>
				<tbody>
					<? 
					for($i=0;$i<$maxDays;$i++) { ?>
					<tr>
						<? $dateToShow =  date('d-m-Y', strtotime($StartDate . ' +'.$i.' day')); ?>
						<td><?=$i+1;?></td>
						<td><?=$dateToShow;?></td>
						<td><?=$MarketName;?></td>
						<? $arrReturn =  getProductData($con, $counter , $dateToShow , $arrProductInfo, $_POST);
							echo $arrReturn["returnString"];
							$AllarrReturn[$i] = $arrReturn;
						?>
					</tr>
					<? } ?> 
					<tr>
						<td>&nbsp;</td>
						<th>Total</th>
						<td>&nbsp;</td>
						<?php
							$OverAllTotal = 0;
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:right;">
								<?
								$catSum = 0;
								for($j=0;$j<$maxDays;$j++) {
									$catSum = $catSum + $AllarrReturn[$j]["Cat_totalquantity"][$i];
								}
								$OverAllTotal = $OverAllTotal + $catSum;
								echo $catSum;
								?>
								</th><? } ?>
						<th style="text-align:right;"><?=$OverAllTotal;?></th>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<th>VALUE RS</th>
						<td>&nbsp;</td>
						<?php
							$OverAllTotal = 0;
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:right;">
								<?
								$catSum = 0;
								for($j=0;$j<$maxDays;$j++) {
									$catSum = $catSum + $AllarrReturn[$j]["Cat_totalcost"][$i];
								}
								$OverAllTotal = $OverAllTotal + $catSum;
								echo $catSum;
								?>
								</th><? } ?>
						<th style="text-align:right;"><?=$OverAllTotal;?></th>
					</tr>
					
				 </tbody>
			</table>
		</div>
	</div>
</div>
 
 