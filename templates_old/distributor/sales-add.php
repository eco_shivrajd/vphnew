<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php include "../includes/distributor_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Sales Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="sales.php">Sales Person</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Sales Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Sales Person
							</div>
							
						</div>
    <?php
if(isset($_POST['submit']))
{
if($_POST['assign'] != "")
{
$external_id=$_POST['assign'];
}
else
{
$external_id=$_SESSION['user_id'];	
}	
$surname="";
$name=$_POST['firstname'];
$username=$_POST['email'];//$name."@sales.com";
//$address=$_POST['address'];
//$state=$_POST['state'];
//$city =$_POST['city'];
$email= $_POST['email'];
//$mobile=$_POST['mobile'];
$user_type="SalesPerson";
$rand="vanraj123";
$password=md5($rand);
//$suburbnm = $_POST['suburbnm'];
$sql1 = mysqli_query($con,"INSERT INTO tbl_user (`external_id`,`surname`,`firstname`,`username`,`pwd`,`user_type`,`email`) 
VALUES('".$external_id."','".$surname."','".$name."','".$username."','".$password."','".$user_type."','".$email."')");
 $userid=mysqli_insert_id($con); 

//check whether user already exists
$sql1="SELECT * FROM tbl_users where id='".$userid."'";
$result1 = mysqli_query($conmain,$sql1);
if(mysqli_num_rows($result1)>0){
	
$sql1="SELECT * FROM tbl_user_company where userid='".$userid."' AND companyid='".CompID."'";
$result1 = mysqli_query($conmain,$sql1);
if(mysqli_num_rows($result1)==0){
//CompID
$sql1 = mysqli_query($conmain,"INSERT INTO tbl_user_company(userid,companyid) 
VALUES('".$userid."','".CompID."')");
}
}
else{
$sql1 = mysqli_query($conmain,"INSERT INTO tbl_users (`id`, `emailaddress`, `passwd`, `username`, `level`) 
VALUES('".$userid."','".$email."','".$password."','".$username."','".$user_type."')");
$sql1="SELECT * FROM tbl_user_company where userid='".$userid."' AND companyid='".CompID."'";
$result1 = mysqli_query($conmain,$sql1);
if(mysqli_num_rows($result1)==0){
//CompID
$sql1 = mysqli_query($conmain,"INSERT INTO tbl_user_company(userid,companyid) 
VALUES('".$userid."','".CompID."')");
}
}
$subject   = "Account Created";
$fromMail  = "admin@vanraj.com";
$semi_rand = md5(time()); 
$headers = "";
$headers .= "From: ".$fromMail."\r\n";
//$headers .= "BCC: mymail@gmail.com\r\n";
$headers .= "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-Type: text/html;" . "\r\n";
$message = "";
$message .= "Hi, <br/>";
$message .= "Your account created successfully<br/>";
$message .= "Please find the login details below<br/>";
$message .= "Username: $username<br/>";
$message .= "Password: $rand<br/>";
//$message .= "Password: eco123<br/>";
$message .= "<a href='https://vanraj.salzpoint.net/templates/login.php'>Click Here</a> to Login<br/>";
$message .= "<br/><br/>";
$message .= "Thanks,<br/>";
$message .= "Admin.";
if($username != ""){
//$sent = @mail($username,$subject,$message,$headers);
$sent = @mail($username,$subject,$message,$headers);
		}
		echo '<script>alert("Sales person added successfully.");location.href="sales.php";</script>';
}	

?> 						<div class="portlet-body">
                       <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                         
                  <form class="form-horizontal" role="form" data-parsley-validate="" method="post" action="sales-add.php">                              
            <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="firstname" 
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control">
              </div>
            </div><!-- /.form-group -->
            
            <!--<div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address."
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9,./() ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control"></textarea>
              </div>
            </div>--><!-- /.form-group -->
                       
            <!--<div class="form-group">
              <label class="col-md-3">State:<span class="mandatory">*</span></label>

              <div class="col-md-4">
             <select name="state" class="form-control" 
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
              onChange="showUser(this.value)">
              <option selected disabled>-select-</option>
<?php
/*$sql="SELECT * FROM tbl_state where country_id=101";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
echo "<option value='$cat_id'>" . $row['name'] . "</option>";
}*/
?>
</select>
              </div>
            </div>--><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">Email:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="email" onBlur="checkAvailability()" id="email"
				 placeholder="Enter E-mail"
		         data-parsley-maxlength="100"
		         data-parsley-maxlength-message="Only 100 characters are allowed."
                 data-parsley-type="email"
                 data-parsley-type-message="Please enter valid e-mail"		   
		         data-parsley-required-message="Please enter e-mail"
                 data-parsley-trigger="change"
                 data-parsley-required="true"
				class="form-control"><span id="user-availability-status"></span>
              </div>
            </div><!-- /.form-group -->
            
            
            <!--<div class="form-group">
              <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="mobile" 
				placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number."
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control">
              </div>
            </div>--><!-- /.form-group -->
            
   
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="sales.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
<style>
.form-horizontal{
font-weight:normal;
}
</style>
<script>  
/*function showUser(str)
{
if (str=="")
{
document.getElementById("Subcategory").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch.php?cat_id="+str,true);
xmlhttp.send();
}

function showSuburb(str)
{
if (str=="")
{
document.getElementById("Subcategory2").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Subcategory2").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch_suburb.php?cat_id="+str,true);
xmlhttp.send();
}*/
function checkAvailability() {
jQuery.ajax({
url: "../includes/ajax_emailcheck.php",
data:'email='+$("#email").val(),
type: "POST",
success:function(data){
$("#user-availability-status").html(data);
},
error:function (){}
});
}
</script> 