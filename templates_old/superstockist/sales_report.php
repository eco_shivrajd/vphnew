<!-- BEGIN HEADER -->
<?php include "../includes/header.php";

function weekDayToTime($week, $year, $dayOfWeek = 0) {
	$dayOfWeekRef = date("w", mktime (0,0,0,1,4,$year));
	if ($dayOfWeekRef == 0) $dayOfWeekRef = 7;
	$resultTime = mktime(0,0,0,1,4,$year) + ((($week - 1) * 7 + ($dayOfWeek - $dayOfWeekRef)) * 86400);
	$resultTime = cleanTime($resultTime);  //Cleaning daylight saving time hours
	return $resultTime;
};

function cleanTime($time) {
	//This function strips all hours, minutes and seconds from time.
	//For example useful of cleaning up DST hours from time
	$cleanTime = mktime(0,0,0,date("m", $time),date("d", $time),date("Y", $time));
	return $cleanTime;
}
function weeks($year)
{   
	return date("W",mktime(0,0,0,12,28,$year));
}


?>
<!-- END HEADER -->
 
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"> </div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/superstockist_sidebar.php"?>
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			Sales Report History <small>Sales Report</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Sales Report</a>
					</li>
				</ul>

			</div>
			<!-- END PAGE HEADER-->
			
			<div class="row">
				<div class="col-md-12"> 
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">Search Criteria</div>
						</div>
						
						<div class="portlet-body">
							<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
								
							<div class="form-group">
								<label class="col-md-3">Report Type:</label>
								<div class="col-md-4">
								
									<input type="radio" name="reportType" id="reportType_daily" value="daily" checked onclick="fnChangeReportType('daily');"> Daily 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_weekly" value="weekly" onclick="fnChangeReportType('weekly');"> Weekly 
									&nbsp;&nbsp;
									<input type="radio" name="reportType" id="reportType_monthly" value="monthly" onclick="fnChangeReportType('monthly');"> Monthly 
								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divDaily" style="display:none;">
								<label class="col-md-3">Select date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="<?php echo date('d-m-Y');?>">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divWeekly" style="display:none;">
								<label class="col-md-3">Select Week:</label>
								<div class="col-md-4">
									<select name="drpWeeklyOption" id="drpWeeklyOption" class="form-control">
									
										<? $yearStart = 2017; $yearEnd = date("Y");
										for($year=$yearStart;$year<=$yearEnd;$year++) {
											for($i=1;$i<=weeks($year);$i++)
											{
												$start = weekDayToTime($i, $year);
												$end   = cleanTime(518400 + $start);
												$selected = '';
												if(weekDayToTime(date("W"), date("Y")) == $start)
												{
													$selected = "selected = 'selected'";
												}
												
												$startDate = strftime("%d-%m-%Y", $start);
												$endDate   = strftime("%d-%m-%Y", $end);
												
												echo "<option value='".$startDate . "::" . $endDate . "' $selected>".strftime("%d-%m-%Y", $start)." To ".strftime("%d-%m-%Y", $end)."</option> \n";

												if($selected!="")
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group" id="divMonthly" style="display:none;">
								<label class="col-md-3">Select Month:</label>
								<div class="col-md-4">
									<select name="drpMonthlyOption" id="drpMonthlyOption" class="form-control">
										<?  
										$yearStart = 2017; $yearEnd = date("Y");
										$currentMonth = date("m-Y") ;
										for($year=$yearStart;$year<=$yearEnd;$year++) { 
											for ($m=1; $m<=12; $m++) {
												$optionValue = "";
												if($m<10)
													$optionValue="0".$m . '-' . $year;
												else 
													$optionValue=$m . '-' . $year;
												
												$selected = "";
												if($currentMonth==$optionValue)
													$selected = " selected = 'selected'";
												
												echo '<option value="' . $optionValue . '" '.$selected.'>' . date('M', mktime(0,0,0,$m)) . '-' . $year . '</option>';
												if($currentMonth==$optionValue)
													break;
											}
										} ?>
										</select> 
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->

							<input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="<?=$_SESSION["user_id"];?>">
									 
							<div class="form-group">
							
								<label class="col-md-3">Stockist:</label>
								<div class="col-md-4" id="divStocklistDropdown">
								 <select name="dropdownStockist" id="dropdownStockist" class="form-control" onchange="fnShowSalesperson(this)">
									<option value="">-Select-</option>
									<?php
									$user_type="Distributor";
									$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION["user_id"]."' order by firstname";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										echo "<option value='$assign_id' >" . $row['firstname'] . "</option>";
									}
									?>
									</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php
										$user_type="SalesPerson";
										$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";
										$result1 = mysqli_query($con,$sql);
										while($row = mysqli_fetch_array($result1))
										{
											$assign_id=$row['id'];
											echo "<option value='$assign_id'>" . $row['firstname'] . "</option>";
										}?>
									</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">State:</label>
								<div class="col-md-4">
									<select name="dropdownState" id="dropdownState" class="form-control" onchange="FnGetCityDropDown(this)">
										<option value="">-Select-</option>
										<?php
										$sql="SELECT name,id FROM tbl_state where country_id=101 order by name";
										$result = mysqli_query($con,$sql);
										while($row = mysqli_fetch_array($result))
										{
											$state_id=$row['id'];
											echo "<option value='$state_id'>" . $row['name'] . "</option>";
										}
										?>
									</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">City:</label>
								<div class="col-md-4" id="divCityDropdown">
								 <select name="dropdownCity" id="dropdownCity" class="form-control" onchange="FnGetSuburbDropDown(this)">
									<option value="">-Select-</option>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Suburb:</label>
								<div class="col-md-4" id="divSuburbDropdown">
								 <select name="dropdownSuburbs" id="dropdownSuburbs" class="form-control" onchange="FnGetShopsDropdown(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT id , suburbnm FROM tbl_surb order by suburbnm";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{	
										echo "<option value='".$row["id"]."'>" . $row["suburbnm"] . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4" id="divShopdropdown">
								 <select name="dropdownshops" id="dropdownshops" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_shops ORDER BY name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . $row["name"] . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Brand:</label>
								<div class="col-md-4">
								 <select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT name , id FROM tbl_brand order by name";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . $row["name"] . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . $row["categorynm"] . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
							
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php											
									$sql="SELECT productname , id   FROM tbl_product order by productname";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										echo "<option value='".$row["id"]."'>" . $row["productname"] . "</option>";
									}
									?>
								</select>
								</div>
							</div><!-- /.form-group -->
														
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnChangeReportType('daily');">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>							
						
						</div>
					   <div class="clearfix"></div>
					</div>
					
					<div id="divReportHTML"></div>
			
		</div>			
	</div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>

<script>
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
};

function fnShowStockist(id) { 	
	var url = "getStockistDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divStocklistDropdown");	
	document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";
}

function FnGetCityDropDown(id) {
	var url = "getCityDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divCityDropdown");	
	/*document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";*/
}

function fnShowSalesperson(id) {
	var url = "getSalesPersonDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divsalespersonDropdown");	 
}

function FnGetShopsDropdown(id) {
	var url = "getShopDropdown.php?suburbid="+id.value;
	CallAJAX(url,"divShopdropdown");
}

function fnShowCategories(id) {
	var url = "getCategoryDropdown.php?brandid="+id.value;
	CallAJAX(url,"divCategoryDropDown");
} 
function FnGetSuburbDropDown(id) {
	if(id.value!="") {
		var url = "getSuburDropdown.php?cityId="+id.value;
		CallAJAX(url,"divSuburbDropdown");
	}
}

function fnShowProducts (id) {
	var url = "getProductDropdown.php?cat_id="+id.value;
	CallAJAX(url,"divProductdropdown");
}

function ShowReport() {
	
	var reportType = $('input[name=reportType]:checked', '#frmsearch').val();
	var url = "dailyReport.php";
	switch(reportType) {
		case "daily" :
			url = "dailyReport.php";
		break;
		case "weekly" :
			url = "weeklyReport.php";
		break;
		case "monthly" :
			url = "monthlyReport.php";
		break;
	}
	
	var data = $('#frmsearch').serialize();
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
		$("#divReportHTML").show();
		$('#divReportHTML').html(response);
	}).fail(function () { });
	
	return false;
}

function fnChangeReportType(reportType) {
	$("#divReportHTML").hide();
	switch(reportType) {
		case "daily" :
			$("#divDaily").show();
			$("#divWeekly").hide();
			$("#divMonthly").hide();
			break;
		case "weekly" :
			$("#divDaily").hide();
			$("#divWeekly").show();
			$("#divMonthly").hide();
			break;
		case "monthly" :
			$("#divDaily").hide();
			$("#divWeekly").hide();
			$("#divMonthly").show();
			break;
	}
}

$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});

fnChangeReportType('daily');

</script>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>