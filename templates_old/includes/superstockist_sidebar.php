<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<br/>
				
				<li class="active open">
					<a href="index.php">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
                
                <li>
					<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Manage Supply Chain</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="distributors.php">
							 Stockist</a>
						</li>
                        <li>
							<a href="sales.php">
							 Sales Person </a>
						</li>
						<li>
							<a href="shops.php">
							 Shops</a>
						</li>
						
					</ul>
				</li>
                
                <li class="">

					<a href="Orders.php">

					<i class="icon-home"></i>

					<span class="title">Orders</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li class="active">
							<a href="Orders.php">
							 Orders</a>
						</li>
                        <li>
							<a href="PlacedOrders.php">
							 Placed Orders </a>
						</li>
						<!--<li>
							<a href="shops.php">
							 Shops</a>
						</li>-->
						
					</ul>
				</li>
               <!-- <li class="">
					<a href="reports.php">
					<i class="icon-home"></i>
					<span class="title">Reports</span>
					<span class="selected"></span>
					</a>
				</li>-->
				
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
					   <li>
							<a href="reports.php">Sales Statistic</a>
						</li>
						<li>
							<a href="sales_report.php">Sales Report</a>
						</li> 
					</ul>
				</li>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>