<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
$id=$_GET['id'];
$name=$_POST['suburbnm'];
$state=$_POST['state'];
$city=$_POST['city'];
$update_sql=mysqli_query($con,"UPDATE tbl_surb SET suburbnm='$name',stateid='$state',cityid='$city' where id='$id'");
echo '<script>location.href="suburb.php";</script>';
}
?>

<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Suburb
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="suburb.php">Suburb</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Suburb</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Suburb
							</div>
							
						</div>
						<div class="portlet-body">
<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                        
  <?php
$id=$_GET['id'];
$sql1="SELECT * FROM tbl_surb where id = '$id' ";
$result1 = mysqli_query($con,$sql1);

while($row1 = mysqli_fetch_array($result1))
{
?>                         
  <form class="form-horizontal" role="form" method="post" data-parsley-validate="" action="">       
            
            <div class="form-group">
              <label class="col-md-3">State:<span class="mandatory">*</span></label>
              <div class="col-md-4">
              <select name="state" 
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
              class="form-control">
<?php
$sql="SELECT * FROM tbl_state where country_id=101";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['stateid'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
}
?>
                </select>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
              <label class="col-md-3">City:<span class="mandatory">*</span></label>
              <div class="col-md-4">
              <select name="city" 
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select city."
              class="form-control">
              <option selected disabled>-select-</option>
<?php
$sql="SELECT * FROM tbl_city where state_id BETWEEN 1 AND 41 ORDER BY name";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['cityid'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
}
?>
               </select>
              </div>
            </div><!-- /.form-group --> 
                <div class="form-group">
              <label class="col-md-3">Suburb Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="suburbnm"
                placeholder="Enter Suburb Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter suburb name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9- ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control" value="<?php echo $row1['suburbnm']?>">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
               <button name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="suburb.php" class="btn btn-primary">Cancel</a>
                <!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
              </div>
            </div><!-- /.form-group -->
            
            

          </form>  
                            
 		  
	  	  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="shop_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>		  
                            
<?php } ?>                           
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>