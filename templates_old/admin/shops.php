<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php include "../includes/sidebar.php"?>	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Shops
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Shops</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Shop Listing
							</div>
                            <a href="shops-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Shop
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									 Shop Name
								</th>
								<!-- <th>
									 Address
								</th> -->
                                <th>
									 Contact Person
								</th>
                                <th>
									 Mobile Number
								</th>
								<th>
									City
								</th>
								<th>
									State
								</th>
							</tr>
							</thead>
							<tbody>
<?php
$sql="SELECT * FROM `tbl_shops`";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{
						
							echo '<tr class="odd gradeX">
								<td>
									 <a href="shops1.php?id='.$row['id'].'">'.$row['name'].'</a>
								</td>';
                                /*<td>
									 '.$row['address'].'
								</td>*/
                                echo '<td>
                                     '.$row['contact_person'].'
								</td>
                                <td>
                                       '.$row['mobile'].'
								</td>
                                <td>';
$city_id=$row['city'];						
$sql="SELECT * FROM tbl_city where id = $city_id";
$result = mysqli_query($con,$sql);
while($num = mysqli_fetch_array($result))
{ 
echo  $num['name'];
}									
								echo 
								'</td>
                                <td>';
$state_id=$row['state'];						
$sql="SELECT * FROM tbl_state where id = $state_id";
$result = mysqli_query($con,$sql);
while($num = mysqli_fetch_array($result))
{ 
echo  $num['name'];
}
								
   						echo		'</td>
							</tr>';

							
}
?>
	
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>